/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: November 16, 2018
//
// Course: CSE002
// Section: 110
// Lab 09 (lab09)
// 
public class ArrayOperations{//declaring class
	public static void main(String[] args){//declaring main method
		int[] testArray = {1,2,3,4,5,6,7,8};//declaring and initialziing initial array
		int[] array0 = copy(testArray);//declaring and initializing first array
		int[] array1 = copy(testArray);//declaring second array
		int[] array2 = new int[testArray.length];//decalring and intializing second array
		print(inverter(array0));//printing inverted array0
		print(inverter2(array1));//printing inverted array1
		array2 = inverter2(testArray);//inverting original array
		print(array2);//printing inverted array
	}
	public static int[] copy(int[] input){//copy array
		int[] newArray = new int[input.length];
		for(int i=0;i<input.length;++i){//copying individual elements using for loop
			newArray[i] = input[i];
		}
		return newArray;//returning copied array
	}
	public static int[] inverter(int[] input){//first inverter loop
		for(int i=0;i<(input.length)/2;++i){//inverting all the elements of the array using for loop
			int tempValue = input[i];
			input[i] = input[(input.length)-i-1];
			input[(input.length)-i-1] = tempValue;
		}
		return input;//returning inverted array
	}
	public static int[] inverter2(int[] input){//second inverter loop
		input = copy(input);//copying array
		input = inverter(input);//invertting the copy
		return input;//returning the inverted array
	}
	public static void print(int[] input){//print arrray
  		for(int i=0;i<input.length;++i){//printing all the elements in the array
  			System.out.print(input[i]+" ");
  		}
  		System.out.println();
	}//end of main method
}//end of classs