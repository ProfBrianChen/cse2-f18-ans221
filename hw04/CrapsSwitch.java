//////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 23, 2018
//
// Course: CSE002
// Section: 110
// Homework 04 (hw04)[switch method]
//
//This program simulates rolling of two dice and outputs the score along with the name of the outcome
//This program uses the switch method 
// 
import java.util.Scanner;//imports Scanner class

//define a class
public class CrapsSwitch{
  //define main method
	public static void main(String[] args){
		Scanner myOwnScanenr = new Scanner(System.in);
		char response;//declaring variable for response
		int firstDiceValue = 0;//declaring and initializing variable for the value of the first dice
		int secondDiceValue = 0;//declaring and initializing variable for the value of the second dice
		int sum = 0;//declaring and initializing the value of the sum of the two values of the dice
		String nameOfDiceOutcome = "";

		System.out.println("Do you want the dice to be cast randomly or do you want to specify the two dice? ");//prompting the user for response whether they want random dice or want to choose the values
		System.out.print("Enter 'R' to cast randomly and 'S' to specify the two dice: ");
		response = myOwnScanenr.next().charAt(0);//taking in the response
		switch (response){//cecking for different condition if the response was to randomly select the dice values or manually enter them
		
     //if the response was random
      case ('R'):
			firstDiceValue = (int) ((Math.random()) * 6 + 1);//picking a random number from 1-6 for the first dice
			secondDiceValue = (int) ((Math.random()) * 6 + 1);//picking a random number from 1-6 for the second dice
			sum = firstDiceValue + secondDiceValue;//taking the sum of the two values
			switch(firstDiceValue){//checking for what the first value was
				case 1://if the first value was 1
				switch (sum){//checking for what the sum of the two values was
          //matching the combination of the first and second dice to the name of the outcome in the following lines: 
					case 2:
					nameOfDiceOutcome = "Snake Eyes";
					break;

					case 3:
					nameOfDiceOutcome = "Ace Deuce";
					break;

					case 4:
					nameOfDiceOutcome = "Easy four";
					break;

					case 5:
					nameOfDiceOutcome = "Fever Five";
					break;

					case 6:
					nameOfDiceOutcome = "Easy six";
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					break;
				}
				break;
				case 2://if the first dice was 2
				switch (sum){//checking for what the sum of the two values was
        //matching the combination of the first and second dice to the name of the outcome in the following lines: 
					case 3:
					nameOfDiceOutcome = "Ace Deuce";
					break;

					case 4:
					nameOfDiceOutcome = "Hard four";
					break;

					case 5:
					nameOfDiceOutcome = "Fever Five";
					break;

					case 6:
					nameOfDiceOutcome = "Easy six";
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					break;

					case 8:
					nameOfDiceOutcome = "Easy Eight";
					break;
				}
				break;
				case 3://if the first dice was 3
				switch (sum){//checking for what the sum of the two values was
          //matching the combination of the first and second dice to the name of the outcome in the following lines: 
					case 4:
					nameOfDiceOutcome = "Easy four";
					break;

					case 5:
					nameOfDiceOutcome = "Fever Five";
					break;

					case 6:
					nameOfDiceOutcome = "Hard six";
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					break;

					case 8:
					nameOfDiceOutcome = "Easy Eight";
					break;

					case 9:
					nameOfDiceOutcome = "Nine";
					break;
				}
				break;
				case 4://if the first dice was 4
				switch (sum){//checking for what the sum of the two values was
          //matching the combination of the first and second dice to the name of the outcome in the following lines:
					case 5:
					nameOfDiceOutcome = "Fever Five";
					break;

					case 6:
					nameOfDiceOutcome = "Easy six";
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					break;

					case 8:
					nameOfDiceOutcome = "Hard Eight";
					break;

					case 9:
					nameOfDiceOutcome = "Nine";
					break;

					case 10:
					nameOfDiceOutcome = "Easy Ten";
					break;
				}
				break;
				case 5://if the first dice was 5
				switch (sum){//checking for what the sum of the two values was
          //matching the combination of the first and second dice to the name of the outcome in the following lines:
					case 6:
					nameOfDiceOutcome = "Easy six";
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					break;

					case 8:
					nameOfDiceOutcome = "Easy Eight";
					break;

					case 9:
					nameOfDiceOutcome = "Nine";
					break;

					case 10:
					nameOfDiceOutcome = "Hard Ten";
					break;

					case 11:
					nameOfDiceOutcome = "Yo-leven";
					break;
				}
				break;
				case 6://if the first dice was 6
				switch (sum){//checking for what the sum of the two values was
          //matching the combination of the first and second dice to the name of the outcome in the following lines:
					case 7:
					nameOfDiceOutcome = "Seven out";
					break;

					case 8:
					nameOfDiceOutcome = "Easy Eight";
					break;

					case 9:
					nameOfDiceOutcome = "Nine";
					break;

					case 10:
					nameOfDiceOutcome = "Easy Ten";
					break;

					case 11:
					nameOfDiceOutcome = "Yo-leven";
					break;

					case 12:
					nameOfDiceOutcome = "Boxcars";
					break;
				}
			}
			System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");//outputting the randomly chosen values
			System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");//outputting the score and the name of the outcome
			break;
		case ('S')://if the user chose to enter the values manually
			System.out.print("Enter the the value you want for the first dice: ");//prompting user to input the value of the first dice
			firstDiceValue = myOwnScanenr.nextInt();//inputting the value of the first dice
			System.out.print("Enter the the value you want for the second dice: ");//prompting user to input the value of the second dice
			secondDiceValue = myOwnScanenr.nextInt();//inputting the value of the second dice
			sum = firstDiceValue + secondDiceValue;//taking the sum of the two values
			switch(firstDiceValue){//checking for the value of first dice
				case 1://if the first dice was 1
				switch (sum){//checking for the value of the sum of the two dice values
          //matching the combination of the first and second dice to the name of the outcome in the following lines then outputting the result:
					case 2:
					nameOfDiceOutcome = "Snake Eyes";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 3:
					nameOfDiceOutcome = "Ace Deuce";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 4:
					nameOfDiceOutcome = "Easy four";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 5:
					nameOfDiceOutcome = "Fever Five";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 6:
					nameOfDiceOutcome = "Easy six";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;
					default:
					System.out.println("Have you seen a dice with six sides that has that number in it? \n No? That's what I thought. Enter a valid number this time idiot!");

				}
				break;

				case 2://if the value of the first dice was 2
				switch (sum){//checking for sum of the two values
          //matching the combination of the first and second dice to the name of the outcome in the following lines then outputting the result:  
					case 3:
					nameOfDiceOutcome = "Ace Deuce";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 4:
					nameOfDiceOutcome = "Hard four";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 5:
					nameOfDiceOutcome = "Fever Five";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 6:
					nameOfDiceOutcome = "Easy six";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 8:
					nameOfDiceOutcome = "Easy Eight";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;
					default:
					System.out.println("Have you seen a dice with six sides that has that number in it? \n No? That's what I thought. Enter a valid number this time idiot!");

				}
				break;
				case 3://if the value of the first dice was 3
				switch (sum){//checking for sum of the two values
          //matching the combination of the first and second dice to the name of the outcome in the following lines then outputting the result:
					case 4:
					nameOfDiceOutcome = "Easy four";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 5:
					nameOfDiceOutcome = "Fever Five";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 6:
					nameOfDiceOutcome = "Hard six";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 8:
					nameOfDiceOutcome = "Easy Eight";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 9:
					nameOfDiceOutcome = "Nine";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;
					default:
					System.out.println("Have you seen a dice with six sides that has that number in it? \n No? That's what I thought. Enter a valid number this time idiot!");

				}
				break;
				case 4://if the value of the first dice was 4
				switch (sum){//checking for sum of the two values
          //matching the combination of the first and second dice to the name of the outcome in the following lines then outputting the result:
					case 5:
					nameOfDiceOutcome = "Fever Five";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 6:
					nameOfDiceOutcome = "Easy six";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 8:
					nameOfDiceOutcome = "Hard Eight";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 9:
					nameOfDiceOutcome = "Nine";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 10:
					nameOfDiceOutcome = "Easy Ten";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;
					default:
					System.out.println("Have you seen a dice with six sides that has that number in it? \n No? That's what I thought. Enter a valid number this time idiot!");

				}
				break;
				case 5://if the first value was 5
				switch (sum){//checking for sum of the two values
          //matching the combination of the first and second dice to the name of the outcome in the following lines then outputting the result:
					case 6:
					nameOfDiceOutcome = "Easy six";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 7:
					nameOfDiceOutcome = "Seven out";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 8:
					nameOfDiceOutcome = "Easy Eight";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 9:
					nameOfDiceOutcome = "Nine";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 10:
					nameOfDiceOutcome = "Hard Ten";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 11:
					nameOfDiceOutcome = "Yo-leven";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;
					default:
					System.out.println("Have you seen a dice with six sides that has that number in it? \n No? That's what I thought. Enter a valid number this time idiot!");

				}
				break;
				case 6://if the first value was 6
				switch (sum){//checking for sum of the two values
          //matching the combination of the first and second dice to the name of the outcome in the following lines then outputting the result:
					case 7:
					nameOfDiceOutcome = "Seven out";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 8:
					nameOfDiceOutcome = "Easy Eight";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 9:
					nameOfDiceOutcome = "Nine";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 10:
					nameOfDiceOutcome = "Easy Ten";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 11:
					nameOfDiceOutcome = "Yo-leven";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;

					case 12:
					nameOfDiceOutcome = "Boxcars";
					System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");
					System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");
					break;
					default:
					System.out.println("Have you seen a dice with six sides that has that number in it? \n No? That's what I thought. Enter a valid number this time idiot!");//calling the user an idiot for inputting an invalid entry


				}
				break;
				default:
				System.out.println("Have you seen a dice with six sides that has that number in it? \n No? That's what I thought. Enter a valid number this time idiot!");//calling the user an idiot for inputting an invalid entry

			}
			
			break;
			
		default:
        //calling the user an idiot for inputting an invalid entry
		System.out.println("Invalid entry. Don't be a idiot and try again please!!");
		System.out.println("And do it right this time. Thanks!");
		}
	}//end of main method
}//end of class