/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: November 11, 2018
//
// Course: CSE002
// Section: 110
// Homework 08 (hw08)
// 
import java.util.Scanner;//import Scanner class
public class hw08{ 
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};//array for suits   
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};//array for digits 
		String[] cards = new String[52];//array for the cards
		String[] hand = new String[5];//array for the hand
		int numCards = 5;//the number of cards wanted in a hand 
		int again = 1;//prompt for user 
		int index = 51;//index for the deck
		for (int i=0; i<52; i++){//assiging the cards to the deck 
		  cards[i]=rankNames[i%13]+suitNames[i/13]; 
		} 
		System.out.println();
		printArray(cards);//printing the deck 
		cards = shuffle(cards);//shuffling the cards
		System.out.println("Shuffled:");
		printArray(cards);//printing the shuffled deck 
		System.out.print("\nPlease enter how many cards you want in your hand: ");
  		while(!scan.hasNextInt()){//checking if the input from user was an integer or not
			System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
			String junkWord = scan.next();//"deleting" what the user typed in
			System.out.print("Please enter how many cards you want in your hand: ");//prompting the user to input the length of the Grid
		}
  		numCards = scan.nextInt();//taking in the input for the number of cards wanted in a hand
		while(again == 1){//while loop to get another hand when asked by user
		if(numCards > index + 1){//if number of Cards is greater than the deck creating and shuffling new deck
			for (int i=0; i<52; i++){ 
		  		cards[i]=rankNames[i%13]+suitNames[i/13]; 
		  	}
		  	cards = shuffle(cards);
		  	index = 51;
		} 
		   hand = getHand(cards,index,numCards);//getting cards in a hand
		   System.out.println("Hand:"); 
		   printArray(hand);//printing the hand
		   index = index - numCards;//changing index
		   System.out.println("Enter a 1 if you want another hand drawn");//prompting user for response 
		   again = scan.nextInt(); //taking in user response
		}  
  	}//end of main method
  	public static void printArray(String[] list){
  		for(int i=0;i<list.length;++i){//printing all the elements in the array
  			System.out.print(list[i]+" ");
  		}
  		System.out.println();
  	}//end of printArray method
  	public static String[] shuffle(String[] list){
  		//shuffling the deck of cards 51 times
  		for(int k=0;k<51;k++){
	  		for(int i=0;i<list.length;++i){
	  			//(Math.random() * ((max - min) + 1)) + min
	  			int j = (int)((Math.random()*(51-i))+1);//generating a random index to shuffle from
	  			String tempValue = list[j];//replacing
	  			list[j] = list[i];
	  			list[i] = tempValue;
	  		}
  		}
  		return list;//returing the shuffled deck
  	}//end of shuffle method
  	public static String[] getHand(String[] list, int position, int numOfCards){
  			String[] select = new String[numOfCards];//declaring the hand array
  			for(int j=0;j<numOfCards;++j){//picking the cards from the deck to enter into the hand
  				//taking the cards from the hand starting from the end of the deck onwards
  				select[j] = list[position];
  				position = position-1;
  			}
  		return select;//returning the hand into main method
  	}//end of getHand method
}//end of class