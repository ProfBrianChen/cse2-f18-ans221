//////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 4, 2018
//
// Course: CSE002
// Section: 110
// Homework 01 (hw01)
// Prints the following:          -----------
//                                | WELCOME |
//                                -----------
//                              ^  ^  ^  ^  ^  ^
//                            / \/ \/ \/ \/ \/ \
//                            <-A--N--S--2--2--1->
//                             \ /\ /\ /\ /\ /\ /
//                              v  v  v  v  v  v
// 
// define a class 
public class WelcomeClass{
  // add main method
  public static void main(String args[]){
  // Series of print statements below to make the pattern appear as intended
  // All the print statements have been spaced a lot in the beginning so as to 
  // make the pattern appear somewhat in the center and more aesthetic
  System.out.println("                                -----------");
  System.out.println("                                | WELCOME |");
  System.out.println("                                -----------");
  System.out.println("                              ^  ^  ^  ^  ^  ^ ");
  System.out.println("                             / \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println("                            <-A--N--S--2--2--1-> ");
  System.out.println("                             \\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println("                              v  v  v  v  v  v");
  //Tweet-length autobiography (280 character max)
  System.out.println("My name is Anmol Shrestha. I am an international student from Nepal. My hometown is Lalitpur,Nepal.");
  System.out.println("I am enrolled in the College of Engineering in Lehigh University. I am pursuing a degree in Computer ");
  System.out.println("Science and Engineering with a minor in Physics.");
  }
}//closing off all the brackets