//////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 11, 2018
//
// Course: CSE002
// Section: 110
// Homework 02 (hw02)
//
// Prints the total cost of each of the items: pants, sweatshirts, and pants,
// the total sales tax on each of these items, and total cost of these items including and
// excluding the sales tax on the items
// 
// define a class 
public class Arithmetic{
	public static void main(String args[]){
	//add main method	

//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

double totalCostOfPants;//declaring variable for total cost of the pants
double totalCostOfShirts;//declaring variable for total cost of the shirts
double totalCostOfBelts;//declaring variable for total cost of the belts
double salesTaxOfPants;//declaring variable for total tax on pants
double salesTaxOfShirts;//declaring variable for total tax on shirts
double salesTaxOfBelts;//declaring variable for total tax on belts
double totalCostNoTax;//declaring variable for total cost of the purchases without taxes
double totalSalesTax;//declaring variable for total sales tax on all the purchases
double totalCostWithTax;//declaring variable for total cost of the purchases with taxes

//calculating total cost of each item my multiplying the number of items with price of each item
totalCostOfPants = numPants * pantsPrice;
totalCostOfShirts = numShirts * shirtPrice;
totalCostOfBelts = numBelts * beltCost;
//printing total cost of each item
System.out.println("The total cost of Pants was $"+totalCostOfPants);
System.out.println("The total cost of Sweatshirsts was $"+totalCostOfShirts);
System.out.println("The total cost of Belt was $"+totalCostOfBelts);

//calculating total sales tax on each item by multiplying total cost an item with the tax rate
salesTaxOfPants = totalCostOfPants * paSalesTax;
salesTaxOfPants = (int)(salesTaxOfPants * 100);//running calculations to get cost upto two deciamal places
salesTaxOfPants = salesTaxOfPants / 100;//running calculations to get cost upto two deciamal places

salesTaxOfShirts = totalCostOfShirts * paSalesTax;
salesTaxOfShirts = (int)(salesTaxOfShirts * 100);//running calculations to get cost upto two deciamal places
salesTaxOfShirts = salesTaxOfShirts / 100;//running calculations to get cost upto two deciamal places

salesTaxOfBelts = totalCostOfBelts * paSalesTax;
salesTaxOfBelts = (int)(salesTaxOfBelts * 100);//running calculations to get cost upto two deciamal places
salesTaxOfBelts = salesTaxOfBelts / 100;//running calculations to get cost upto two deciamal places

//adding total cost of each item without taxes to get total cost of all items without taxes	
totalCostNoTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
//printing total cost of all purchases without taxes
System.out.println("The total cost of the purchases (before sales taxes) was $"+totalCostNoTax);

//adding sales tax on all items to get total sales tax 
totalSalesTax = salesTaxOfPants + salesTaxOfShirts + salesTaxOfBelts;
//printing total sales tax on all purchases
System.out.println("The total sales tax from the purchases was $"+totalSalesTax);

//adding total cost excluding taxes with total sales tax to obtain total cost of the purchases including taxes
totalCostWithTax = totalCostNoTax + totalSalesTax;
//printing total cost of the purchases with taxes
System.out.println("The total cost of the purchases (including sales tax) was $"+totalCostWithTax);	
	}//end of main method
}//end of class