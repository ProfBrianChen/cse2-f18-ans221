/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: October 26, 2018
//
// Course: CSE002
// Section: 110
// Lab 07 (lab07)
// 
import java.util.Scanner;//import Scanner class

public class printSentences{
	public static String adjective(){
		int randNum = (int)(Math.random()*10);
		switch(randNum){
			case 0:
			return "Plain";

			case 1:
			return "Quaint";

			case 2:
			return "Inexpensive";

			case 3:
			return "Deep";

			case 4:
			return "Straight";

			case 5:
			return "Wide";

			case 6:
			return "Gigantic";

			case 7:
			return "Greasy";

			case 8:
			return "Abusive";

			case 9:
			return "Loud";
		}
		return "Error";
	}
	public static String subjectNoun(){
		int randNum = (int)(Math.random()*10);
		switch(randNum){
			case 0:
			return "Jamal";

			case 1:
			return "Harry";

			case 2:
			return "Beyonce";

			case 3:
			return "Sara";

			case 4:
			return "Caroline";

			case 5:
			return "Jane";

			case 6:
			return "Nial";

			case 7:
			return "Zayn";

			case 8:
			return "Gerald";

			case 9:
			return "Brad";
		}
		return "Error";
	}
	public static String verb(){
		int randNum = (int)(Math.random()*10);
		switch(randNum){
			case 0:
			return "bid";

			case 1:
			return "bit";

			case 2:
			return "bent";

			case 3:
			return "chose";

			case 4:
			return "burnt";

			case 5:
			return "caught";

			case 6:
			return "came";

			case 7:
			return "cut";

			case 8:
			return "came";

			case 9:
			return "drew";
		}
		return "Error";
	}
	public static String objectNoun(){
		int randNum = (int)(Math.random()*10);
		switch(randNum){
			case 0:
			return "on president.";

			case 1:
			return "on plants.";

			case 2:
			return "on a bike.";

			case 3:
			return "in education.";

			case 4:
			return "on the stomach.";

			case 5:
			return "some liquid.";

			case 6:
			return "on a writer.";

			case 7:
			return "on that vein.";

			case 8:
			return "some knowledge.";

			case 9:
			return "at leather.";
		}
		return "Error";
	}
	public static String support(){
		int randNum = (int)(Math.random()*10);
		switch(randNum){
			case 0:
			return "is a bad boi.";

			case 1:
			return "is a good boi.";

			case 2:
			return "is not a good boi.";

			case 3:
			return "is a lit boi.";

			case 4:
			return "likes to do hobbies.";

			case 5:
			return "has actual hobbies.";

			case 6:
			return "probably goes to college.";

			case 7:
			return "has done some education.";

			case 8:
			return "has no skills at all.";

			case 9:
			return "has next to no skills or aspirations .";
		}
		return "Error";
	}
	public static String conclusion(){
		int randNum = (int)(Math.random()*10);
		switch(randNum){
			case 0:
			return "died yesterday.";

			case 1:
			return "went to the country of Bosina-Herzegovina.";

			case 2:
			return "met the girl of his dreams.";

			case 3:
			return "does not care about anubody else but himself.";

			case 4:
			return "is not a problematic criminal.";

			case 5:
			return "is going to be travelling to his hometown.";

			case 6:
			return "will be dearly missed.";

			case 7:
			return "will live on in our heats and minds.";

			case 8:
			return "should have worked harder for his family's sake.";

			case 9:
			return "could not control himself.";
		}
		return "Error";
	}
	public static String actionSentence(){
		int randNum = (int)(Math.random()*10);
		switch(randNum){
			case 0:
			return " was very broken-hearted when he found out his partner had betrayed them. It was the worst they of their life.";

			case 1:
			return " wasn't very pleased when they discovered about the state of the world. Life's hard for everyone.";

			case 2:
			return " believes that the earth flat. They are an idiot.";

			case 3:
			return " believes that vaccination gives people autism. They are a huge idiot.";

			case 4:
			return " went to the forest and never returned.";

			case 5:
			return " left their family to pursue their dreams. Were they a selfish perosn? Nobody knows!";

			case 6:
			return " is a huge Halsey fan. I am trying really hard to not be judgemental.\n It's really hard!";

			case 7:
			return " doesn't know what they are talking about. They have never been an inelligent person.";

			case 8:
			return " should just keep quiet. Everytime they open their mouh something alwasys goes wrong.";

			case 9:
			return " is having a hard time in college. They also feel homesick. It's fine because they have lots of friends to help them out.";
		}
		return "Error";
	}
	public static void main(String[] args){
		String Sentence = "";
		int response;
		int count=0;
		String name = subjectNoun();
		Scanner scnr = new Scanner(System.in);
		while(count<4){
			if(count==0){
				Sentence += adjective() + " ";
			}
			if(count==1){
				Sentence += name + " ";
			}
			if(count==2){
				Sentence += verb() + " ";
			}
			if(count==3){
				Sentence += objectNoun() + " " + name + " " + support() + " " + name + " " + conclusion() + " " + name + actionSentence();
				System.out.println(Sentence);
			}
			if(count==3){
			System.out.println();
			System.out.print("Type '1' if you want another sentence or '2' if you want to exit this stupid program: ");
			while(!scnr.hasNextInt()){//checking if the input from user was an integer or not
				System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
				String junkWord = scnr.next();//"deleting" what the user typed in
				System.out.print("Type '1' if you want another sentence or '2' if you want to exit this stupid program: ");//prompting the user to input the length of the Grid
			}
			response = scnr.nextInt();
			while(!(response == 1 || response == 2)){
				System.out.println("The integer is not one of the options you idiot!");//scolding the user for input outside the range
				System.out.print("Type '1' if you want another sentence or '2' if you want to exit this stupid program: ");//prompting the user to input the length of the Grid
			while(!scnr.hasNextInt()){//checking if the input from user was an integer or not
				System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
				String junkWord = scnr.next();//"deleting" what the user typed in
				System.out.print("Type '1' if you want another sentence or '2' if you want to exit this stupid program: ");//prompting the user to input the length of the Grid
			}
			response = scnr.nextInt();
			}
			if(response == 1){
				count=0;
				Sentence = "";
				name = subjectNoun();
				System.out.println();

				continue;
			}
			else{
				break;
			}
		}
		count++;
		}
	}
}