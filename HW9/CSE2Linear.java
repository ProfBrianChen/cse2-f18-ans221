/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: November 18, 2018
//
// Course: CSE002
// Section: 110
// Homework 09 (hw09)
//Program 1
// 
import java.util.Scanner;//importing Scanner class

public class CSE2Linear{//declaring class
	public static void main(String[] args){//declaring main method
		Scanner scan = new Scanner(System.in);
		int[] userInput = new int[15];//declaring array
		int key;//declaring the key
		int iterations = 0;//declaring and initializing the number of iterations
		System.out.println("Enter 15 ascending ints for final grades in CSE2:");//prompting the user to input the grades
		for(int i=0;i<userInput.length;++i){//for loop to take in user input
			userInput[i] = AskUser(scan,i);
			if(i!=0 && userInput[i]<userInput[i-1]){//if the value entered is smaller than any previous entries
				System.out.println("Please enter a value greater than any previous value you entered!");
				i--;
				continue;//taking input for the same index again in case the value entered was smaller than previous values
			}
		}
		PrintArray(userInput);//printing the array
		System.out.print("Enter the grade you are looking for: ");//prompting user for key
		key = scan.nextInt();//taking in the key
		iterations = BinarySearch(userInput,key);//searching for the key using the binary method
		if(iterations != -1){//in case the key was found
			System.out.println(key + " was found in the list with " + iterations +" iterations");
		}
		userInput = scramble(userInput);//scrambling the grades
		System.out.println("Scrambled:");
		PrintArray(userInput);//printing the scrambled grades
		System.out.print("Enter the grade you are looking for: ");//prompting user for key
		key = scan.nextInt();//taking in the key
		iterations = LinearSearch(userInput,key);//seraching for the key using linear method
		if(iterations != -1){//in case the key was found
			System.out.println(key + " was found in the list with " + iterations +" iterations");
		}
	}//end of main methood
	public static int AskUser(Scanner scan, int i){//method for getting the key from user 
		int value;//declaring the key 
		System.out.print("Enter the "+(i+1)+" th value of the grid:  ");//prompting the user to input the value of the Grid
		while(!scan.hasNextInt()){//checking if the input from user was an integer or not
			System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
			String junkWord = scan.next();//"deleting" what the user typed in
			System.out.print("Enter the "+(i+1)+" th value of the grid:  ");//prompting the user to input the value of the Grid
		}
		value = scan.nextInt();
		while(!(value >= 0 && value <= 100)){
			System.out.println("The integer is not in the specified range you idiot!");//scolding the user for input outside the range
			System.out.print("Enter the "+(i+1)+" th value of the grid:  ");//prompting the user to input the value of the Grid
			while(!scan.hasNextInt()){//checking if the input from user was an integer or not
				System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
				String junkWord = scan.next();//"deleting" what the user typed in
				System.out.print("Enter the "+(i+1)+" th value of the grid:  ");//prompting the user to input the value of the Grid
			}
			value = scan.nextInt();//finally taking in the input of the user after all the freaking testing
		}
		return value;//returning the key to the main method
	}//end of AskUser method
	public static void PrintArray(int[] list){//method for printing array
		for(int i=0; i<list.length; ++i){
			System.out.print(list[i]+" ");
		}
		System.out.println();
	}//end of PrintArray
	public static int[] scramble(int[] list){//method to scramble 
  		for(int k=0;k<51;k++){//shuffling the array 51 times
	  		for(int i=0;i<list.length;++i){
	  			int j = (int)((Math.random()*(14-i))+1);//generating a random index to shuffle from
	  			int tempValue = list[j];//replacing the values
	  			list[j] = list[i];//replacing
	  			list[i] = tempValue;//replacing
	  		}
  		}
  		return list;//returing the scrambled array
	}
	/* BINARY SEARCH CODE USED WRITTEN IN PROFESSOR CARR'S CLASS*/
	public static int BinarySearch(int[] list, int key){//method for the binary search
		int low = 0;//declaring and initializing the lowest index
		int high = list.length-1;//declaring and initializing the highes index
		int count = 0;//declaring and initializing the the number of iterations
		while(high >= low) {
			count++;//increment in the iteration
			int mid = (low + high)/2;//the middle index point
			if (key < list[mid]) {
				high = mid - 1;
			}
			else if (key == list[mid]) {
				return count;//returning the number of iterations
			}
			else {
				low = mid + 1;
			}
		}
		System.out.println(key + " was not found in the list with " + count + " iterations");//printing that the key wasn't found
		return -1;//returning -1 if key not found
	}//end of BinarySearch method
	/* BINARY SEARCH CODE USED WRITTEN IN PROFESSOR CARR'S CLASS*/
	public static int LinearSearch(int[] list, int key){//method for searching with linear method
		int count = 0;//declaring and initializing the number of iterations
		for(int i=0;i<list.length;++i){
			count++;//increment in the number of iterations
			if(list[i] == key){
				return count;//returning the number of iterations in main method
			}
		}
		System.out.println(key+" was not found in the list with " + count + " iterations");//printing that the key wasn't found
		return -1;//returning -1 if the key wasn't  found
	}//end of LinearSearch method
}//end of class