/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 14, 2018
//
// Course: CSE002
// Section: 110
// Lab 03 (lab03)
// 
import java.util.Scanner;//import Scanner class
//define a class
public class Check{
  //add main method
	public static void main(String args[]){
	Scanner myScanner = new Scanner(System.in);
	System.out.print("Enter the original cost of the check in the form xx.xx: ");//printing the prompt for input
	double checkCost = myScanner.nextDouble();//taking in input for cost of Check
	System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");//printing the prompt for input
	double tipPercent = myScanner.nextDouble();//taking in input for tipPercent
	tipPercent /= 100;//dividing the tip percentage by 100
	System.out.print("Enter the number of people who went out to dinner: ");//printing the prompt for input
	int numPeople = myScanner.nextInt();//taking in input for the total number of people
	double totalCost;//declaring variable for total cost
	double costPerPerson;//declaring variable for cost per person
	int dollars,//whole dollar amount of cost 
      dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
	totalCost = checkCost * (1 + tipPercent);//calculating total cost
	costPerPerson = totalCost / numPeople;//calculating cost per person
//get the whole amount, dropping decimal fraction
	dollars=(int)costPerPerson;//calculating whole number of dollars values
	dimes=(int)(costPerPerson * 10) % 10;//calculating total number of dimes
	pennies=(int)(costPerPerson * 100) % 10;//calcuating the number of pennies
	System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);//printing amount each person has to pay
	}//end of main method
}//end of clas