//////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 18, 2018
//
// Course: CSE002
// Section: 110
// Homework 03 (hw03)
//Part 2
//
//This program takes in input from the user for the lenghts of the square side of the base of the pyramid,
//the height of the pyramid, and calculates then displays the total volume of the pyramid from the entered data
// 

import java.util.Scanner;//imports Scanner class
//define a class
public class Pyramid{
	//add main method
	public static void main(String[] args){
	double squareSide;//declaring the varible for the square side of the base of the pyramid
	double height;//declaring the variable for the height of the pyramid 
	double volume;//declaring the variable for the volume of the pyramid
	Scanner myScanner = new Scanner(System.in);
	System.out.print("Enter the length of the square side of the pyramid: ");//prompting the user to enter the length of  square side of the base of the pyramid
	squareSide = myScanner.nextDouble();//taking in input for the length of the square side of the base of the pyramid
	System.out.print("Enter the length of the height of the pyramid: ");//prompting user to enter the length of the height of the pyramid
	height = myScanner.nextDouble();//taking in input for the length of the height of the pyramid
	volume = (squareSide * squareSide * height) / 3;//calculating the volume of the pyramid
	System.out.println("The voulme inside the pyramid is: " + volume);//printing the volume of the pyramid
	}//end of main method
}//end of class