/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: October 18, 2018
//
// Course: CSE002
// Section: 110
// Homework 06 (hw06)
// 
import java.util.Scanner;//import Scanner class
//define a class
public class EncryptedX{
	//add main method
	public static void main(String[] args){
		Scanner myScanner = new Scanner(System.in);
		int length;//declaring the length of the Grid
		System.out.print("Enter the size of the grid in the range of (1-100): ");//prompting the user to input the length of the Grid
		while(!myScanner.hasNextInt()){//checking if the input from user was an integer or not
			System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
			String junkWord = myScanner.next();//"deleting" what the user typed in
			System.out.print("Enter the size of the grid in the range of (1-100): ");//prompting the user to input the length of the Grid
		}
		length = myScanner.nextInt();
		while(!(length >= 0 && length <= 100)){
			System.out.println("The integer is not in the specified range you idiot!");//scolding the user for input outside the range
			System.out.print("Enter the size of the grid in the range of (1-100): ");//prompting the user to input the length of the Grid
			while(!myScanner.hasNextInt()){//checking if the input from user was an integer or not
				System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
				String junkWord = myScanner.next();//"deleting" what the user typed in
				System.out.print("Enter the size of the grid in the range of (1-100): ");//prompting the user to input the length of the Grid
			}
			length = myScanner.nextInt();//finally taking in the input of the user after all the freaking testing
		}
		//System.out.println(length);//for debugging
		for(int i=0; i<length+1; ++i){//for loop for the size of the grid
			for(int j=0; j<length+1; ++j){//for loop for printing the pattern
				if((j % length == i) || (j % length == (length - i))){//printing whitespaces depending on the position on the grid
					System.out.print(" ");//printing whitespace for the X pattern
				}
				else {//printing the stars
				System.out.print("*");//printing stars for the X pattern
				}
			}
			System.out.println();//goint to next row
		}		
	}//end of main method
}//end of class