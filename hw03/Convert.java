//////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 18, 2018
//
// Course: CSE002
// Section: 110
// Homework 02 (hw02)
//Part 1
//
//This program takes in input from the user for the inches of rainfall and area of the affected region in inches,
//calulates, the total volume of rainfall in AcresInches then converts and displays the volume in cubic miles
// 

import java.util.Scanner;//import Scanner class
//define a class
public class Convert{
	//add main method
	public static void main (String[] args){
	final double gallonsInOneAcresInches = 27154.2857;//declaring constant for number of Gallons per AcresInches
	final double cubicMilesInOneGallon = 9.0817e-13;//declaring constant for number of cubic miles per Gallon
	double totalVolumeOfRain;//declaring variable for total amount of rain in the area
	Scanner myScanner = new Scanner(System.in);
	System.out.print("Enter the affected area in acres: ");//prompting user for input of affected area in Acres
	double affectedArea = myScanner.nextDouble();//taking in input for total affected area in Acres

	System.out.print("Enter the inches of rainfall in the affected area: ");//prompting user for input of amount of rain in inches in that area
	double rainfallAmount = myScanner.nextDouble();// taking in input for amount of rain in inches in that area

	totalVolumeOfRain = affectedArea * rainfallAmount;//calculating the volume of rain in AcresInches
  totalVolumeOfRain = totalVolumeOfRain * gallonsInOneAcresInches * cubicMilesInOneGallon;//converting the volume of rainfall to cubic miles

	System.out.println(totalVolumeOfRain + " cubic miles");//printing the total volume of rainfall to cubic miles
	}//end of main method
}//end of class