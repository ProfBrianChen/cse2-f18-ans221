/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: November 18, 2018
//
// Course: CSE002
// Section: 110
// Homework 09 (hw09)
//Program 2
// 
import java.util.Scanner;//importing Scanner class
public class RemoveElements{//declaring class
  public static void main(String [] args){//declaring main method
	Scanner scan=new Scanner(System.in);//decaling Scanner variable
	int num[]=new int[10];//declaring array for the int
	int newArray1[];//declaring array for the second int
	int newArray2[];//declaring array for the third int
	int index,target;//declaring the index and target we want to look for
	String answer="";//declaring and initializing the string for the response
	do{//do while loop for the process to happen again in case of the required response
	  	System.out.print("Random input 10 ints [0-9]");//printing the random string of arrays
	  	num = randomInput();//printing the random elements in an array
	  	String out = "The original array is:";
	  	out += listArray(num);//printing the array
	  	System.out.println(out);//printing the array
	 
	  	System.out.print("Enter the index ");//prompting user to input the desired index
	  	index = scan.nextInt();//taking in the user input for desire index
	  	newArray1 = delete(num,index);//calling the delete method
	  	String out1="The output array is ";//printing the result of delete method
	  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out1);//printing the new array after delete method is called
	 
	    System.out.print("Enter the target value ");//prompting user to input the target element
	  	target = scan.nextInt();//taking in input for the target element
	  	newArray2 = remove(num,target);//calling remove method
	  	String out2="The output array is ";//printing the result of remove method
	  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out2);//printing the result of remove method
	  	 
	  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");//prompting user to input if they want to go again
	  	answer=scan.next();////taking in the user input of how they want to proceed
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){//method to print out the arrays
	String out="{";//printing "{"
	for(int j=0;j<num.length;j++){//for loop to print each element in the array
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";//printing "}"
	return out;//returnig the String of the printed array
  }
  public static int[] randomInput(){//array for the random elements
	int[] randomArray = new int[10];//declaring the array of random elements
  	for(int i=0;i<randomArray.length;++i){
  		randomArray[i] = (int)(Math.random()*(10));//generating random element for each index
  	}
  	return randomArray;//returning the random array to main method
  }//end of randomInput method
  public static int[] delete(int[] list, int pos){//array for deleting the element at required index
  	if(pos >= 10){//if index is out of bound print error message
  		System.out.println("The index is not valid.");
  		return list;//returning original array if the index entered is invalid
  	}
  	int[] notRandomArray = new int[list.length - 1];//declaring the new array
  	for(int i=0;i<pos;++i){//for loop until the required index
  		notRandomArray[i] = list[i];//copying the initial array up until the wanted index
  	}
  	for(int i=pos;i<notRandomArray.length;++i){//for loop for when index is found and so on
  		notRandomArray[i] = list[i+1];//copying the initial array since and after the required index
  	}
  	return notRandomArray;//retunring the new array to the main method
  }//end of delete method
  public static int[] remove(int[] list, int target){//method to remove specific elements
  	int count = 0;//declaring and initializing the count for total number of elements we are looking for
  	int count2 = 0;//declaring and initializing the count for shift in index
  	for(int i=0;i<list.length;++i){//if the element is found 
  		if(target == list[i]){
  			count++;//increase the number of occurences
  		}
  	}
  	if(count == 0){//if no occurences print error message
  		System.out.println("Element " + target + " was not found");
  		return list;//return original array
  	}
  	System.out.println("Element " + target + " has been found");
  	int[] notRandomArray = new int[list.length-count];//declaring new array depending on the number of occurences
  	for(int i=0;i<list.length;++i){//going through the original list for the removal and formation of new array
  		if(target == list[i]){//if the target element is found
  			count2++;//shift the index
  			continue;//go back to the loop
  		}
  		notRandomArray[i-count2] = list[i];//make the new array depending on the shift on index
  	}
  	return notRandomArray;//returning the new array
  }//end of remove method
}//end of class