//////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 23, 2018
//
// Course: CSE002
// Section: 110
// Homework 04 (hw04)[if-else method]
//
//This program simulates rolling of two dice and outputs the score along with the name of the outcome
//This program uses the if-else method 
// 
import java.util.Scanner;//imports Scanner class

//define a class
public class CrapsIf{
  //add main method
	public static void main(String[] args){
		Scanner myOwnScanenr = new Scanner(System.in);
		char response;//declaring variable for response
		int firstDiceValue = 1;//declaring and initializing variable for the value of the first dice
		int secondDiceValue = 1;//declaring and initializing variable for the value of the second dice
		String nameOfDiceOutcome = "";//declaring and initializing the variable for the name of the outcome

		System.out.println("Do you want the dice to be cast randomly or do you want to specify the two dice? ");//prompting the user for response whether they want random dice or want to choose the values
		System.out.print("Enter 'R' to cast randomly and 'S' to specify the two dice: ");
		response = myOwnScanenr.next().charAt(0);//taking in the response
		if (response == 'R'){//if the response was random
			firstDiceValue = (int) ((Math.random()) * 6 + 1);//picking a random number from 1-6 for the first dice
			secondDiceValue = (int) ((Math.random()) * 6 + 1);//picking a random number from 1-6 for the second dice
			
      //matching the combination of the first and second dice to the name of the outcome in the following lines: 
      if(firstDiceValue == 1 && secondDiceValue == 1){
					nameOfDiceOutcome = "Snake Eyes";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 2) || (firstDiceValue == 2 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Ace Deuce";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 3) || (firstDiceValue == 3 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Easy Four";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 4) || (firstDiceValue == 4 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Fever Five";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 5) || (firstDiceValue == 5 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Easy Six";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Seven out";
				}
				else if (firstDiceValue == 2 && secondDiceValue == 2){
					nameOfDiceOutcome = "Hard four";
				}
				else if ((firstDiceValue == 2 && secondDiceValue == 3) || (firstDiceValue == 3 && secondDiceValue == 2)){
					nameOfDiceOutcome = "Fever five";
				}
				else if ((firstDiceValue == 2 && secondDiceValue == 4) || (firstDiceValue == 4 && secondDiceValue == 2)){
					nameOfDiceOutcome = "Easy six";
				}
				else if ((firstDiceValue == 2 && secondDiceValue == 5) || (firstDiceValue == 5 && secondDiceValue == 2)){
					nameOfDiceOutcome = "Seven out";
				}
				else if ((firstDiceValue == 2 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 2)){
					nameOfDiceOutcome = "Easy Eight";
				}
				else if(firstDiceValue == 3 && secondDiceValue == 3){
					nameOfDiceOutcome = "Hard six";
				}
				else if ((firstDiceValue == 3 && secondDiceValue == 4) || (firstDiceValue == 4 && secondDiceValue == 3)){
					nameOfDiceOutcome = "Seven out";
				}
				else if ((firstDiceValue == 3 && secondDiceValue == 5) || (firstDiceValue == 5 && secondDiceValue == 3)){
					nameOfDiceOutcome = "Easy Eight";
				}
				else if ((firstDiceValue == 3 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 3)){
					nameOfDiceOutcome = "Nine";
				}
				else if (firstDiceValue == 4 && secondDiceValue == 4){
					nameOfDiceOutcome = "Hard Eight";
				}
				else if ((firstDiceValue == 4 && secondDiceValue == 5) || (firstDiceValue == 5 && secondDiceValue == 4)){
					nameOfDiceOutcome = "Nine";
				}
				else if ((firstDiceValue == 4 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 4)){
					nameOfDiceOutcome = "Easy Ten";
				}
				else if (firstDiceValue == 5 && secondDiceValue == 5){
					nameOfDiceOutcome = "Hard Ten";
				}
				else if ((firstDiceValue == 5 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 5)){
					nameOfDiceOutcome = "Yo-leven";
				}
				else if (firstDiceValue == 6 && secondDiceValue == 6){
					nameOfDiceOutcome = "Boxcars";
				}
				System.out.println("Your first dice was " + firstDiceValue + " and your second dice was " + secondDiceValue + " so ");//outputting the randomly chosen values
				System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");//outputting the score and the name of the outcome
		}
		else if (response == 'S'){//if the user chose to manually enter the values of the dice
			System.out.print("Enter the the value you want for the first dice: ");//prompting user to input the value of the first dice
			firstDiceValue = myOwnScanenr.nextInt();//inputting the value of the first dice
			System.out.print("Enter the the value you want for the second dice: ");//prompting user to input the value of the second dice
			secondDiceValue = myOwnScanenr.nextInt();//inputting the value of the second dice
			
      //matching the combination of the first and second dice to the name of the outcome in the following lines:
      if ((firstDiceValue >= 1 && firstDiceValue <= 6) && (secondDiceValue >= 1 && secondDiceValue <= 6)){
				if(firstDiceValue == 1 && secondDiceValue == 1){
					nameOfDiceOutcome = "Snake Eyes";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 2) || (firstDiceValue == 2 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Ace Deuce";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 3) || (firstDiceValue == 3 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Easy Four";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 4) || (firstDiceValue == 4 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Fever Five";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 5) || (firstDiceValue == 5 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Easy Six";
				}
				else if ((firstDiceValue == 1 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 1)){
					nameOfDiceOutcome = "Seven out";
				}
				else if (firstDiceValue == 2 && secondDiceValue == 2){
					nameOfDiceOutcome = "Hard four";
				}
				else if ((firstDiceValue == 2 && secondDiceValue == 3) || (firstDiceValue == 3 && secondDiceValue == 2)){
					nameOfDiceOutcome = "Fever five";
				}
				else if ((firstDiceValue == 2 && secondDiceValue == 4) || (firstDiceValue == 4 && secondDiceValue == 2)){
					nameOfDiceOutcome = "Easy six";
				}
				else if ((firstDiceValue == 2 && secondDiceValue == 5) || (firstDiceValue == 5 && secondDiceValue == 2)){
					nameOfDiceOutcome = "Seven out";
				}
				else if ((firstDiceValue == 2 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 2)){
					nameOfDiceOutcome = "Easy Eight";
				}
				else if(firstDiceValue == 3 && secondDiceValue == 3){
					nameOfDiceOutcome = "Hard six";
				}
				else if ((firstDiceValue == 3 && secondDiceValue == 4) || (firstDiceValue == 4 && secondDiceValue == 3)){
					nameOfDiceOutcome = "Seven out";
				}
				else if ((firstDiceValue == 3 && secondDiceValue == 5) || (firstDiceValue == 5 && secondDiceValue == 3)){
					nameOfDiceOutcome = "Easy Eight";
				}
				else if ((firstDiceValue == 3 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 3)){
					nameOfDiceOutcome = "Nine";
				}
				else if (firstDiceValue == 4 && secondDiceValue == 4){
					nameOfDiceOutcome = "Hard Eight";
				}
				else if ((firstDiceValue == 4 && secondDiceValue == 5) || (firstDiceValue == 5 && secondDiceValue == 4)){
					nameOfDiceOutcome = "Nine";
				}
				else if ((firstDiceValue == 4 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 4)){
					nameOfDiceOutcome = "Easy Ten";
				}
				else if (firstDiceValue == 5 && secondDiceValue == 5){
					nameOfDiceOutcome = "Hard Ten";
				}
				else if ((firstDiceValue == 5 && secondDiceValue == 6) || (firstDiceValue == 6 && secondDiceValue == 5)){
					nameOfDiceOutcome = "Yo-leven";
				}
				else if (firstDiceValue == 6 && secondDiceValue == 6){
					nameOfDiceOutcome = "Boxcars";
				}
				System.out.println("Your score is "+ (firstDiceValue + secondDiceValue) + " and the name of the outcome is " + nameOfDiceOutcome + ".");//outputting the score and the name of the outcome
			}
			else{
				System.out.println("Have you seen a dice with six sides that has that number in it? \n No? That's what I thought. Enter a valid number this time idiot!");//calling the user an idiot for inputting an invalid entry
			}
		}
		else{
      //calling the user an idiot for inputting an invalid entry
			System.out.println("Invalid entry. Don't be a idiot and try again please!!");
			System.out.println("And do it right this time. Thanks!");
		}
		
	}//end of main method
}//end of class
