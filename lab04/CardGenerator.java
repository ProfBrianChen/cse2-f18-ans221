/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 21, 2018
//
// Course: CSE002
// Section: 110
// Lab 04 (lab04)
// 
public class CardGenerator{
	public static void main(String[] args) {
		int randomNumber = (int) ((Math.random()) * 52 + 1);//generatiing a random integer from 1 to 52
		//System.out.println(randomNumber);//for debugging purposes
		String suit;//declaring String for suit
		String identity;//declaring String for identity of card
		int remainder = randomNumber % 13;//finding the remainder when the random number is divided by 13  to find it's identiy
		if (randomNumber >=1 && randomNumber <= 13){//1-13 is Diamonds
			suit = "Diamonds";
		}
		else if (randomNumber >= 14 && randomNumber <= 26){//14-26 is Diamonds
			suit = "Clubs";
		}
		else if (randomNumber >= 27 && randomNumber <= 39){//27-39 is Diamonds
			suit = "Hearts";
		}
		else {//40-52 is Diamonds
			suit = "Spades";
		}
		switch(remainder){
			//if remainder is 1 then it must be an Ace
			case 1:
			identity = "Ace";
			break;
			// if remainder is 2 then it must be a 2
			case 2:
			identity = "2";
			break;
			// if remainder is 3 then it must be a 3
			case 3:
			identity = "3";
			break;
			// if remainder is 4 then it must be a 4
			case 4:
			identity = "4";
			break;
			// if remainder is 5 then it must be a 5
			case 5:
			identity = "5";
			break;
			// if remainder is 6 then it must be a 6
			case 6:
			identity = "6";
			break;
			// if remainder is 7 then it must be a 7
			case 7:
			identity = "7";
			break;
			// if remainder is 8 then it must be a 8
			case 8:
			identity = "8";
			break;
			// if remainder is 9 then it must be a 9
			case 9:
			identity = "9";
			break;
			// if remainder is 10 then it must be a 10
			case 10:
			identity = "10";
			break;
			// if remainder is 11 then it must be a Jack
			case 11:
			identity = "Jack";
			break;
			// if remainder is 12 then it must be a Queen
			case 12:
			identity = "Queen";
			break;
			// if remainder is 0 then it must be a King
			case 0:
			identity = "King";
			break;
			//convert the integer to String for identity
			default:
			identity = Integer.toString(randomNumber);
		}
		System.out.println("You picked the " + identity + " of " + suit);//printing out the random card obtained
	}//end of main method
}//end of class