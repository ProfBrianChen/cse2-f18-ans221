//////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: October 8, 2018
//
// Course: CSE002
// Section: 110
// Homework 05 (hw05)
//
//This program generates random poker hands and uses the hands generated with the random cards to calculate the probablitiy
//of getting four-of-a-kind, three-of-a-kind, two-pairs, and one-pair
// 
import java.util.Scanner;//import Scanner class 
import java.util.Arrays;//import Arrays class

public class Hw05{
	public static void main(String [] args){
		int numberOfHands;//declaring variable for number of hands
		Scanner scnr = new Scanner(System.in);
		int count = 1,//declaring variable for counter for the number of hands
		    numberOfPairs = 0,//declaring and initializing for the counter of number of pairs
		    countFourOfAKind = 0,//declaring and initializing for the counter of number of four of a kind
		    countThreeOfAKind = 0,//declaring and initializing for the counter of number of three of a kind
		    countTwoPair = 0,//declaring and initializing for the counter of number of two pairs
		    countOnePair = 0;//declaring and initializing for the counter of number of one pairs
		System.out.print("Enter the number of hands you want to be generated: ");//prompts the user to enter the number of hands
		while(!scnr.hasNextInt()){//checking if the user inputted an integer
			System.out.println("Excuse you!! Does that look like and integer to you? No? Try again.");//calling the user an idiot for wrong input
			System.out.print("Enter the number of hands you want to be generated: ");//prompting the user to enter the number of hands again because the user is an idiot
			String junkWord = scnr.next();
		}
		numberOfHands = scnr.nextInt();//taking in input for the number of hands
		while(count <= numberOfHands){
			int oneHand[] = {100, 101, 102, 103, 104};//initializing the array for the poker hands
			for(int i = 1; i<=5; i++){
				int randomNumber = (int) ((Math.random()) * 52 + 1);//generatiing a random integer from 1 to 52
				oneHand[i-1] = randomNumber;//entering the random card into the array of the hands
        //checking if the same card was generated for the hands
				if(oneHand[0] == oneHand[1] ||
				   oneHand[0] == oneHand[2] || 
				   oneHand[0] == oneHand[3] || 
				   oneHand[0] == oneHand[4] || 
				   oneHand[1] == oneHand[2] || 
				   oneHand[1] == oneHand[3] || 
				   oneHand[1] == oneHand[4] || 
				   oneHand[2] == oneHand[3] || 
				   oneHand[2] == oneHand[4] || 
				   oneHand[3] == oneHand[4]){
					oneHand[0] = 100;
					oneHand[1] = 101;
					oneHand[2] = 102;
					oneHand[3] = 103;
					oneHand[4] = 104;
					i = 0;//if the same cards are generated go back and start generating cards for the hands from the beginning
					continue;
				}
			}
      //checking digits of the cards in the hands
			int remainder1 = oneHand[0] % 13;
			int remainder2 = oneHand[1] % 13;
			int remainder3 = oneHand[2] % 13;
			int remainder4 = oneHand[3] % 13;
			int remainder5 = oneHand[4] % 13;
			//int remainderArray[] = {5,5,3,3,1};//for debugging
      //cheking if there are any pairs in a hand using nested for loop
			int remainderArray[] = {remainder1, remainder2, remainder3, remainder4, remainder5};
			for(int i = 0; i< 5; i++){
				for(int j = i+1; j< 5; j++){
					if(remainderArray[i] == remainderArray[j]){
						numberOfPairs++;
					}
				}
			}
			if(numberOfPairs == 1){//counting number of one pairs
				countOnePair++;
			}
			else if(numberOfPairs == 2){//counting number of two pairs
				countTwoPair++;
			}
			else if(numberOfPairs == 3){//counting number of three of a kinds
				countThreeOfAKind++;
			}
			else if(numberOfPairs == 6){//counting number of four of a kinds
				countFourOfAKind++;
			}
			//System.out.println(countOnePair);//debugging
			//System.out.println(Arrays.toString(oneHand));//debugging
			//System.out.println(Arrays.toString(remainderArray));//debugging
			//System.out.println(numberOfPairs);//debugging
			count++;//incrememnting to the next hand
		}
		double probablityOnePair = (double)(countOnePair) / (double) (numberOfHands);//calculating the probablitiy
		double probablityTwoPair = (double)(countTwoPair) / (double) (numberOfHands);//calculating the probablitiy
		double probablityThreeOfAKind = (double)(countThreeOfAKind) / (double) (numberOfHands);//calculating the probablitiy
		double probablityFourOfAKind = (double)(countFourOfAKind) / (double) (numberOfHands);//calculating the probablitiy
		System.out.println("The number of loops: " + numberOfHands);//printing the probability
		System.out.printf("The probablitiy of Four-of-a-kind: %1.3f\n",probablityFourOfAKind);//printing the probability
		System.out.printf("The probablitiy of Three-of-a-kind: %1.3f\n",probablityThreeOfAKind);//printing the probability
		System.out.printf("The probablitiy of Two-pair: %1.3f\n",probablityTwoPair);//printing the probability
		System.out.printf("The probablitiy of One-pair: %1.3f\n",probablityOnePair);//printing the probability
	}//end of main method
}//end of class