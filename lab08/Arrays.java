/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: November 9, 2018
//
// Course: CSE002
// Section: 110
// Lab 08 (lab08)
// 

public class Arrays{//declare class
	public static void main(String[] args){//add main method
		int[] firstArray = new int[100];//declaring first Array
		int[] secondArray = new int[100];//declaring second Array
		int counter=0;//declaring and initializing counter
		int sum=0;//declaring and initializing sum
		for(int i=0;i<100;++i){//for loop to assign first array
			//(Math.random() * ((max - min) + 1)) + min
			firstArray[i] = (int)(Math.random()*100);//generating random number for each element
		}
		for(int i=0;i<100;++i){//for loop for element of first array
			for(int j=0;j<100;++j){//for loop for second array elements
				if(i==firstArray[j]){//checking if the occurenences
					counter++;//increment in arrays
				}
			}
			secondArray[i]=counter;//putting the number of occurences
			counter=0;//resetting the counter
		}
		for(int i=0;i<100;++i){//for loop for printing for each second array value
			String print ="";//String to print time or times
			if(secondArray[i]==1 || secondArray[i]==0){//assigning print depending on number of occurences
				print=" time";
			}
			else{
				print=" times";
			}
			sum += secondArray[i];
			System.out.println(i+" occurs "+ secondArray[i] + print);//printing the number of occurences
		}
		//System.out.println(sum); //for debugging
	}//end of main method
}//end of class