/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: October 27, 2018
//
// Course: CSE002
// Section: 110
// Homework 07 (hw07)
// 
import java.util.Scanner;//import Scanner class
import java.util.regex.Pattern;//import Pattern class
import java.util.regex.Matcher;//import Matcher class

public class WordTools{//define a class
	public static String sampleText(Scanner scnr){//add sampleText method
		System.out.println("Enter a sample text: ");//prompting user for the sample text
		return scnr.nextLine();//taking in the text and retunring it in main method
	}//end of sampleText method

	public static char printMenu(Scanner scnr){//add printMenu method
		System.out.println("\nMENU\nc - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit\nChoose an option:");//prompting the user for response on their stupid text
		String input = scnr.next();//taking in the response
		//checking if the user was an idiot and inputted an invalid response
		while(!(input.charAt(0) == 'c'  || input.charAt(0) == 'w' || input.charAt(0) == 'f' || input.charAt(0) == 'r' || input.charAt(0) == 's' || input.charAt(0) == 'q' || input.length() < 1)){
			System.out.println("That is not one of the options on the menu you idiot!");//scolding the user for input outside the range
			System.out.println("\nWhat do you want to do with your text? ");//asking the user what they want to do
			System.out.println("\nMENU\nc - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit\nChoose an option:");//prompting the user to input a response
			input = scnr.next();//taking in the response again
		}
		return input.charAt(0);//taking in the user's response and returning it to main method
	}//end of printMenu method

	public static int getNumOfNonWSCharacters(String check){//add getNumOfNonWSCharacters method
		int count = 0;//count for the number of non white space characters
		for(int i = 0; i<check.length(); ++i){//going through all the index in the string
			if(!(Character.isWhitespace(check.charAt(i)))){
				count++;//incrementing the count for every non white space character
			}
		}
		return count;//returning the number of non white space characters to main method
	}//end of getNumOfNonWSCharacters method

	public static int getNumOfWords(String check){//add getNumOfWords method
		int count = 0;//count for number of words
		for(int i = 0; i<check.length(); ++i){//going through all the index of the string
			if(Character.isWhitespace(check.charAt(i))) {
				if(!(Character.isWhitespace(check.charAt(i+1)))){//a word is present between two white spaces
				count++;//increment in the number of words
				}
			}
		}
		if(Character.isWhitespace(check.charAt(0))){//if the first letter is a space then an extra word is counted to returning the number of words accordingly
			return count;
		}
		else{//the last word is not counted becuase there is no whitespace in the end so returning the number of words accordingly
			return count+1;
		}
	}//end of getNumOfWords method

	public static int findText(String toFind, String check){//add findText method
		int count = 0;//count for the number of occurences
		Pattern p = Pattern.compile(toFind);//using Pattern class
		Matcher m = p.matcher( check );//using Matcher class
		while (m.find()) {
		    count++;//increment in the number of occurences
		}
		return count;//retunring the number of occurences to main method
	}//end of findText method

	public static String replaceExclamation(String check){//add replaceExclamation method
		return check.replace('!','.');
	}//end of replaceExclamation method

	public static String shortenSpace(String check){//add shortenSpace method
		String shorterSentence = check;//taking in the user text
		int count = 0;//count for the number of whitespace
		int j = check.length();//taking the length of the initial text
		int k;//declaring variable for the new index after each replpacement
		for(int i = 0; i<j; ++i){//going through all the index in the string
			if(Character.isWhitespace(shorterSentence.charAt(i))){//if the letter is a whitespace increase count and continue to loop
				count++;
				continue;
			}
			k = i - count;//getting the adjusted index size for replacement
			if (Character.isWhitespace(shorterSentence.charAt(k))){//when all the whitespaces have been accounted for by the previous if statement and the last letter of the substring is whitespace proceed with the replacement
				shorterSentence = shorterSentence.replace((shorterSentence.substring(k,count+k))," ");
			}
			count = 0;//revert the count to 0 for next string of whitespaces
			j = shorterSentence.length();//taking in the new length of the string after the change
		}
		return shorterSentence;//return the new edited String to main method
	}//end of shortenSpace method

	public static void main(String[] args){//add of main method
		Scanner scnr = new Scanner(System.in);//declaring Scanner variable
		String userInputedText = sampleText(scnr);//taking in user text
		System.out.println("\nYou eneterd:\n" + userInputedText);//echoing the user input
		System.out.println("\nWhat do you want to do with your text? ");//asking the user what they want to do with their stupid text
		char response = printMenu(scnr);//taking in user response
		while (response != 'q'){//if the user did not quit
			switch(response){//depending on the response
				case 'c'://calling and executing getNumOfNonWSCharacters method
				System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(userInputedText));
				break;

				case 'w'://calling and executing getNumOfWords method
				System.out.println("Number of words: " + getNumOfWords(userInputedText));
				break;

				case 'f'://calling and executing findText method
				System.out.print("Enter a word or phrase to be found: ");//asking the user for the word of phrase they want to find
				String junkWord = scnr.nextLine();//taking in the junk input
				String wordOrPhraseToFind = scnr.nextLine();//taking in the phrase or word to be found
				System.out.println("\"" + wordOrPhraseToFind + "\""+ " instances: " + findText(wordOrPhraseToFind,userInputedText));
				break;

				case 'r'://calling and executing replaceExclamation method
				userInputedText = replaceExclamation(userInputedText);
				System.out.println("Edited text:\n" + userInputedText);
				break;

				case 's'://calling and executing shortenSpace method
				userInputedText = shortenSpace(userInputedText);
				System.out.println("Edited text:\n" + userInputedText);
				break;
			}
			System.out.println("\nWhat else would you like to do with your stupid text? ");//asking the user what else they want to do with their stupid text
			response = printMenu(scnr);//taking in another response from the user
		}
		System.out.println("\nOk! I see you! Bye!!!");//bidding the user farewell 'cause why not'
	}//end of main method
}//end of class