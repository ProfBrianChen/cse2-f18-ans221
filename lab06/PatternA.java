/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: October 12 , 2018
//
// Course: CSE002
// Section: 110
// Lab 06 (lab06)
//PatternA
// 
import java.util.Scanner;//import Scanner class
//define a class
public class PatternA{
	//add main method
	public static void main(String[] args){
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Enter the dimension in integer in the range of (1-10): ");//prompting the user to input the length of the Twist
		int length = 0;//declaring the length of the Twist
		int count = 1;//declaring and initializing the number of counts for the while loop
		while(!myScanner.hasNextInt()){//checking if the input from user was an integer or not
			System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
			String junkWord = myScanner.next();//"deleting" what the user typed in
			System.out.print("Enter the dimension in integer in the range of (1-10): ");//prompting the user to input the length of the Twist
		}
		length = myScanner.nextInt();
		while(!(length >= 0 && length <= 10)){
			System.out.println("The integer is not in the specified range you idiot!");
			System.out.print("Enter the dimension in integer in the range (1-10): ");
			while(!myScanner.hasNextInt()){//checking if the input from user was an integer or not
				System.out.println("That is not an integer idiot! Enter an INTEGER this time!");//scolding the user for invalid input
				String junkWord = myScanner.next();//"deleting" what the user typed in
				System.out.print("Enter the dimension in integer in the range of (1-10): ");//prompting the user to input the length of the Twist
			}
			length = myScanner.nextInt();//taking in the value of the dimension
		}
		//printing the pyramid pattern
		for(int i=0; i<length; ++i){//this loop is for the size of the pyramid
			for(int j=0; j<=i; ++j){//this loop is for what is being printed out
				System.out.print((j+1)+" ");
			}
			System.out.println();
		}
		//System.out.println(length);//for debugging 
	}//end of main method
}//end of class