/////////////////////////////////////////////
// Anmol Shrestha
// Lehigh ID: ans221
// Lehigh ID number: 873668950
// Date: September 7, 2018
//
// Course: CSE002
// Section: 110
// Lab 02 (lab02)
// 
// define a class 
public class Cyclometer {
  // add main method
	public static void main(String args[]){
		double secsTrip1 = 480;//number of secs for trip1
		int secsTrip2 = 3220;//number of secs for trip2
		int countsTrip1 = 1561;//number of rotations for trip1
		int countsTrip2 = 9037;//number of rotations for trip2
		double wheelDiameter = 27.0;//diameter of the wheel
		double PI=3.14159;//constant value of pi
		int feetPerMile = 5280;//number of feets in a mile
		int inchesPerFoot = 12;//number of inches in a foot
		double secondsPerMinute = 60;//number of seconds in a minute
		double distanceTrip1;//declaring the distance covered in trip1
		double distanceTrip2;//declaring the distance covered in trip2
		double totalDistance;////declaring the total distance covered 
		System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");//printing 
		System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");//printing 
		distanceTrip1 = countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile;//disatnce covered in trip1
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
		distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//disatnce covered in trip2
		totalDistance = distanceTrip1 + distanceTrip2;//total distance covered
		System.out.println("Trip 1 was "+distanceTrip1+" miles");//printing 
		System.out.println("Trip 2 was "+distanceTrip2+" miles");//printing 
		System.out.println("The total distance was "+totalDistance+" miles");//printing 
	}//end of main method
}//end of class